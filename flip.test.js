const request = require('supertest')('https://gorest.co.in/public/v2');
const expect = require('chai').expect;

describe('Code once, runs forever, Chapter 3.1', function() {
  
  it('Count todos', async function() {
    const response = await request
      .get('/todos')
      expect(response.body.length).to.equal(20)
  });

  it('Assert status inactive', async function() {
    const response = await request
      .get('/users?status=inactive')
    const count = response.body.length
    for (let i = 0; i < count; i++) {
      expect(response.body[i].status).to.equal("inactive")
    }    
  });

});
